package com.demo.JavaCloudPRNT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 *  @author joshuaventocilla
 *  A CLOUDPRNT SAMPLE FOR JAVA USING SPRINGBOOT WEB API
 *  DISCLAIMER: CODE IS NOT OFFICIAL STAR MICRONICS© CODE IS WRITTEN FOR SAMPLE PURPOSES ONLY
 */

@SpringBootApplication
@ComponentScan({"com.demo.JavaCloudPRNT", "com.demo.JavaCloudPRNT.service"})
public class JavaCloudPrntApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaCloudPrntApplication.class, args);
	}

}
