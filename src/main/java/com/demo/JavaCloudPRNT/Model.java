package com.demo.JavaCloudPRNT;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public class Model {
	
	String[] mediaTypes;
	String status;
	String statusCode;
	String printerMAC;
	
	boolean jobReady;
	boolean printingInProgress;
	
	public Model() {
		// Constructor
	}
	
	public Model(boolean jobReady, boolean printingInProgress, String[] mediaTypes, String status,
			String statusCode, String printerMAC) {
		this.mediaTypes = mediaTypes;
		this.jobReady   = jobReady;
		this.printingInProgress = printingInProgress;
		this.status     = status;
		this.statusCode = statusCode;
		this.printerMAC = printerMAC;
	}
	
	public Model(boolean jobReady, String[] mediaTypes) {
		this.jobReady   = jobReady;
		this.mediaTypes = mediaTypes;
	}
	
	public boolean getJobReady() {
		return jobReady;
	}
	
	public void setJobReady(boolean jobReady) {
		this.jobReady = jobReady;
	}
	
	public boolean getPrintingProgress() {
		return jobReady;
	}
	
	public void setPrintingProgress(boolean printInProgress) {
		this.printingInProgress = printInProgress;
	}
	
	public String[] getMediaTypes() {
		return mediaTypes;
	}
	
	public void setMediaTypes(String[] mediaTypes) {
		this.mediaTypes = mediaTypes;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setPrinterStatus(String status) {
		this.status = status;
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getPrinterMAC() {
		return printerMAC;
	}
	
	public void setPrinterMAC(String printerMAC) {
		this.printerMAC = printerMAC;
	}

}
