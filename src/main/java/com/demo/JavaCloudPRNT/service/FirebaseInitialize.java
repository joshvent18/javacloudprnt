package com.demo.JavaCloudPRNT.service;

import java.io.FileInputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Configuration

@Service
@Component
@Repository
public class FirebaseInitialize {
	
	@PostConstruct
	public void initialize() throws IOException {
		try {
			FileInputStream serviceAccount =
					new FileInputStream("./serviceAccount.json");

					FirebaseOptions options = new FirebaseOptions.Builder()
					  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
					  .setDatabaseUrl("https://cloudprnt-e068f.firebaseio.com")
					  .build();

					FirebaseApp.initializeApp(options);
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

}
