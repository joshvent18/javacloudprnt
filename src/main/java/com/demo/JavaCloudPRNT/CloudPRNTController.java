package com.demo.JavaCloudPRNT;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLEngineResult.Status;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.demo.JavaCloudPRNT.Model;
import com.google.firebase.internal.FirebaseService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import jakarta.servlet.http.HttpServletRequest;

@RestController
public class CloudPRNTController {
	
	// Run using localhost:8080/cloudprnt
	
	//	@Autowired
	//	FirebaseService firebaseService;
	
	 private static final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		
	@RequestMapping(value = "/addJob", method = {RequestMethod.POST}) 
	public String createPrintJob(@RequestBody Model model) {
		return "hello";
	}

	@RequestMapping(value = "/initial", method = {RequestMethod.GET})
	public String intial() {
		return "Java CloudPRNT Example";
	}
	
	/*
	 * STEP 1 & 2
	 * POST REQUEST / RESPONSE
	 */	
	@RequestMapping(value = "/cloudprnt", method = {RequestMethod.POST}) 
	public ResponseEntity<Model> postResponse(@RequestBody Model model,
											  HttpServletRequest request) {
		
		
		try {
			String command = "/Users/joshuaventocilla/Documents/Self/JavaCloudPRNT " + 
	  				"cputil decode application/vnd.star.starprnt markup.stm outputdata.bin";
			
			Process p = Runtime.getRuntime().exec(command);
			
			File file = new File("outputdata.bin");
            file.createNewFile();
			
			System.out.println("Running cputil... ");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Failed to run cputil..");
			e1.printStackTrace();
		}
				
		///
		/// *********** SET MEDIA TYPE *********
		///
		String[] mediaTypes  ={ "application/vnd.star.starprnt" };
				
		HttpHeaders responseHeaders = new HttpHeaders();
		Enumeration<String> headerNames = request.getHeaderNames();
		
		// JSON model to string
		Model response       = new Model();
		Gson gson            = new Gson();
		String decodedStatus = "";
		String jsonString    = gson.toJson(model);
		JsonObject obj       = new Gson().fromJson(jsonString, JsonObject.class);
							
//		// Get headers
//		if (headerNames != null) {
//			while (headerNames.hasMoreElements()) {
//				System.out.println("Header: " + request.getHeader(headerNames.nextElement()));
//		    }
//			System.out.println("\n");
//		}
		
//		System.out.println("JSON: " + jsonString);
		
//		System.out.println("**********     " + request.getMethod() + "    " + request.getRequestURL());
//		System.out.println("\n\nAuthorization: " + request.getHeader("Authorization"));
		
		try {
			decodedStatus = URLDecoder.decode(model.statusCode, "UTF-8").replaceAll("\\s.*", "");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
						
		if(obj.get("printingInProgress").getAsBoolean()) {
			System.out.println("Printing is in progress... \n\n");
			return new ResponseEntity<Model>(response, HttpStatus.OK);
		} else {
			
			int statusCode = Integer.parseInt(decodedStatus);
			
//			System.out.println("JSON:\t" + jsonString);
//			System.out.println("STATUS:\t");
			
			System.out.println(statusCode);
			
			switch (statusCode) {
				case 200:		
					if(request.getHeader("User-Agent").contains("CloudPRNT/3.0 TSP100IV/1.1")) {
						System.out.println("TSP100IV IS DENIED");
					} else {
						
						response = beginPostResponse(mediaTypes);
						System.out.println("OK. Printer is online.");
//						System.out.println(request.getHeader("User-Agent"));
					}
					response = beginPostResponse(mediaTypes);
					System.out.println("OK. Printer is online.");
					break;
					case 201:
						response = beginPostResponse(mediaTypes);
						System.out.println("Output paper has been taken and printer is able to print.");
						break;
					case 211:	
						response = beginPostResponse(mediaTypes);
						System.out.println("Paper low.");
						break;
					case 220:
						System.out.println("Printing in progress...");
					case 221:
						System.out.println("Output paper is present");
						break;
					case 410:
						System.out.println("Out of paper");
						break;
					case 411:
						System.out.println("Paper jam");
						break;
					case 420:
						System.out.println("Cover open");
						break;
					case 510:
						System.out.println("Incompatible media type");
						break;
					case 520:
						System.out.println("Timeout");
						break;
					case 521:
						System.out.println("Download failed. File is too large.");
						break;
					default:
						System.out.println("Unknown Printer Status");
				}
			
//			System.out.println("Sending POST response...\n\n");
			
			return new ResponseEntity<Model>(response, HttpStatusCode.valueOf(statusCode));
		}
					
//			** UNCOMMENT IF USING FOR HTTP CHALLENGE **
//			-- SEND HTTP CHALLENGE IF USERNAME AND PASSWORD IS SET --
//			-- Note: Must send 'Unauthorized' otherwise challenge will not be sent
//
//			if (request.getHeader("Authorization") != null) {
//				return new ResponseEntity<Model>(response, HttpStatus.OK);
//			} else {
//				responseHeaders.set("WWW-Authenticate", "Basic realm=\"Authentication Required\"");
//				return new ResponseEntity<Model>(responseHeaders, HttpStatus.UNAUTHORIZED);
//			}
			
	}
		
	/*
	 * Step 3 & 4
	 * HTTP GET REQUEST / RESPONSE
	 * Respond with file based on 'mediaTypes'
	 */
		
	@RequestMapping(value = "/cloudprnt", method = {RequestMethod.GET})
	public ResponseEntity<byte[]> getResponse(HttpServletRequest request) throws IOException {
		HttpHeaders responseHeaders = new HttpHeaders();
		String contentType = request.getParameter("type");
		String status      = request.getParameter("printingInProgress");
		
		System.out.println("**********     " + request.getMethod() + "    " + request.getRequestURL() + request.getQueryString());
				
		// print data based on the media type from the request
		if (contentType.equalsIgnoreCase("text/plain")) {
			responseHeaders.set("X-Star-CashDrawer", "start");
			responseHeaders.set("Content-type", "text/plain");
		} else if (contentType.equalsIgnoreCase("image/png")) {
			responseHeaders.setContentType(MediaType.IMAGE_PNG);
			
			// This is set in case image is dithering
			responseHeaders.add("X-Star-ImageditherPattern", "none");
			responseHeaders.set("X-Star-ImageDitherPattern", "none");
		} else {
			responseHeaders.add("Content-type", "application/vnd.star.starprnt");
			responseHeaders.set("Content-type", "application/vnd.star.starprnt");
		}
		
		System.out.println("Sending GET response...\n");
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println("GETTING job on " + timestamp +"\n\n");
				
		return new ResponseEntity<byte[]>(printData(contentType), responseHeaders, HttpStatus.OK);
	}
	
	/*
	 * STEP 5
	 * HTTP DELETE
	 */
	@RequestMapping(value = "/cloudprnt", method = {RequestMethod.DELETE})
	public ResponseEntity<Void> deleteRequest(HttpServletRequest request) {
		System.out.println("**********     " + request.getMethod() + "    " + request.getRequestURL() + request.getQueryString());
		
		String status = request.getParameter("code").substring(0, 3);	
		int statusCode = Integer.parseInt(status);
		
		System.out.println("PRINTING COMPLETED... DELETING.");
		
		switch(statusCode) {
			case 200:
				// OK, delete the current job
				System.out.println(statusCode + "\tPrint job success. Delete job.");
				break;
			case 211:
				// OK, Paper low
				System.out.println(statusCode + "\tPrint job success. Paper is low, please change receipt rolls. Delete job.");
				break;
			case 410:
				System.out.println(statusCode + "\tOut of paper, please change receipt rolls.");
				break;
			case 511:
				// Unsupported media, Media decoding error
				// Clear the job
				System.out.println(statusCode + "\tUnsupported Media. Media Decoding Error");
				break;
			default:
					// some other result
				System.out.println(statusCode + "\tDefault: " + request.getQueryString());
		}	
		
		System.out.println("Sending DELETE response...\n");
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println("DELETED job on " + timestamp + "\n\n");
		
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
	
	 ////////////////// HELPER METHODS /////////////
	
	/***
 	 * This should be set on the fly; if a job exits. 
	 * This example does not check this, and assumes a job exists.
	 * { 
	 * 		jobReady: TRUE, 
	 * 		mediaType: 'text/plain' 
	 * }
	 */
	 public Model beginPostResponse(String[] mediaTypes) {
		 Model model = new Model();
		 String[] media = mediaTypes;
		 
		 model.jobReady = true;
		 model.mediaTypes = media;
		 
		 return model;
	 }

	 /***
	  * Print data based on the mediaType
	  * This could be text/plain, image/png, image/jpg, etc....
	  * Visit following link for list of media types:
	  * https://www.star-m.jp/products/s_print/CloudPRNTSDK/Documentation/en/developerguide/contentmediatypes/contentmediatypes_overview.html
	  * 
	  * @param	mediaType	a list of media types to accept
	  * @return				data in byte array
	  */
	public byte[] printData(String mediaType) {
		File file;
		byte[] result = null;
		
		if(mediaType.contentEquals("text/plain")) {
			file = new File("sample.txt");
			String bufferedString;
			
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				bufferedString = br.readLine();
				br.close();
				result = bufferedString.getBytes();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if(mediaType.contentEquals("image/png")) {
			file = new File("grubhubtest.png");
			result = readImage(file, "png");
		} else {
			file = new File("outputdata.bin");
			
			try {
				InputStream input = new BufferedInputStream(new FileInputStream(file));
				result = readAndClose(input);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
//		String encoded = "G0AbUgAbHkYAGyAAG2wAG3oBGx0pVQIAMAEbHSlVAgBAABsdYQENChtpAQENChtFQ2FzaCBUaXAgVHJhbnNhY3Rpb24NChtpAAANChtGDQpUZXN0IFBPUw0KMzAxIENvbmdyZXNzLCBTdWl0ZSAzMDUsIEF1c3RpbiwgVFggNzg3NzcNCjUxMjk5OTg4NTUNCkRhdGU6IDAyLzA1LzIwMjQgMDQ6MzRQTQ0KDQoNCj09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PQ0KGx1BAABTdGFmZhsdQfgBQW1vdW50DQo9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0NChsdQQAATmFkYXYgR2l2b25pGx1B+AEkMTIuMDANCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLQ0KGx1hAg0KVG90YWwgJDEyLjAwDQoNCg0KLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tDQobHWEADQpDYXNoaWVyOiBOYWRhdg==";
//		result = Base64.getDecoder().decode(encoded);
						
		return result;	
	}
	
	 /**
	  *		Read an input stream, and return it as a byte array.  
	  * 	Sometimes the source of bytes is an input stream instead of a file. 
	  * 	This implementation closes aInput after it's read.
	  **/
	  byte[] readAndClose(InputStream input){
	    //carries the data from input to output :    
	    byte[] bucket = new byte[32*1024]; 
	    ByteArrayOutputStream result = null; 
	    try  {
	      try {
	        //Use buffering? No. Buffering avoids costly access to disk or network;
	        //buffering to an in-memory stream makes no sense.
	        result = new ByteArrayOutputStream(bucket.length);
	        int bytesRead = 0;
	        while(bytesRead != -1){
	          //aInput.read() returns -1, 0, or more :
	          bytesRead = input.read(bucket);
	          if(bytesRead > 0){
	            result.write(bucket, 0, bytesRead);
	          }
	        }
	      }
	      finally {
	        input.close();
	        //result.close(); this is a no-operation for ByteArrayOutputStream
	      }
	    }
	    catch (IOException ex){
	      System.out.println(ex);
	    }
	    return result.toByteArray();
	  }
	  
	  /**
	   *	Read image and return it as a byte array.  
	   *	Sometimes the source of bytes is an input stream instead of a file. 
	   *	This implementation closes aInput after it's read.
	   */
	  
	  byte[] readImage(File imageFile, String format) {
		  
		  byte[] imageData = new byte[32*1024]; 
		  
		  try {
			BufferedImage bImage = ImageIO.read(imageFile);
			ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
			ImageIO.write(bImage, format, bOutput);
			imageData = bOutput.toByteArray();
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
		  
		  return imageData;
	  }
	  
	  public String getBody(HttpServletRequest request)  {

		    String body = null;
		    StringBuilder stringBuilder = new StringBuilder();
		    BufferedReader bufferedReader = null;

		    try {
		        InputStream inputStream = request.getInputStream();
		        if (inputStream != null) {
		            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		            char[] charBuffer = new char[128];
		            int bytesRead = -1;
		            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
		                stringBuilder.append(charBuffer, 0, bytesRead);
		            }
		        } else {
		            stringBuilder.append("");
		        }
		    } catch (IOException ex) {
		        // throw ex;
		        return "";
		    } finally {
		        if (bufferedReader != null) {
		            try {
		                bufferedReader.close();
		            } catch (IOException ex) {

		            }
		        }
		    }

		    body = stringBuilder.toString();
		    return body;
		}
	  
	  public static void executeCputil() throws IOException, InterruptedException {
		  String command = "/Users/joshuaventocilla/Documents/Self/JavaCloudPRNT " + 
				  				"cputil decode application/vnd.star.starprnt markup.stm outputdata.bin";
		 Process p = Runtime.getRuntime().exec(command);
	  }
	  
		// return mediaType as 'text/plain'
//		@RequestMapping(value = "/cloudprnt", method = {RequestMethod.GET})
//		public ResponseEntity<String> getResponse(HttpServletRequest request) throws IOException {
//			
//			HttpHeaders responseHeaders = new HttpHeaders();
//			responseHeaders.set("Content-type", "text/plain");
//			
//			String contentType = request.getParameter("type");
//			String strResponse = null;
//			
//			System.out.println(request.getMethod() + "    " + request.getRequestURL() + request.getQueryString());
//			
//			File file = new File("sample.txt");
//			
//			try {
//				BufferedReader br = new BufferedReader(new FileReader(file));
//				strResponse = br.readLine();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			System.out.println("Sending GET response...");
//					
//			return new ResponseEntity<String>(strResponse, responseHeaders, HttpStatus.OK);
	//
//		}
	  
		// mediaType as 'application/vnd.star.starprnt' using output file
		// generated by cputil
//	    @RequestMapping(value = "/cloudprnt", method = {RequestMethod.GET})
//		public ResponseEntity<byte[]> getResponse() {
//			
//			HttpHeaders responseHeaders = new HttpHeaders();
//			responseHeaders.add("Content-type", "application/vnd.star.starprnt");
//			responseHeaders.set("Content-type", "application/vnd.star.starprnt");
//			
//			File file = new File("outputdata.bin");
//			byte[] result = null;
//			
//			try {
//				InputStream input = new BufferedInputStream(new FileInputStream(file));
//				result = readAndClose(input);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			return new ResponseEntity<byte[]>(result, responseHeaders, HttpStatus.OK);
//		}
		
		// mediaType as 'image/vnd.star.png'
//		@RequestMapping(value = "/cloudprnt", method = { RequestMethod.GET })
//		public ResponseEntity<byte[]> getResponse(HttpServletRequest request) {
//			
	//
//			System.out.println(request.getMethod() + "    " + request.getRequestURL() + request.getQueryString());
//			
//			HttpHeaders responseHeaders = new HttpHeaders();
//			responseHeaders.setContentType(MediaType.IMAGE_PNG);
//			
//			File file     = new File("test.png");
//			byte[] result = readImage(file, "png");
//					
//			return new ResponseEntity<byte[]>(result, responseHeaders, HttpStatus.OK);
//			
//		}
	  
//		if (request.getAttribute("Content-Type") == "application/json") {
//			return new ResponseEntity<Model>(response, HttpStatus.OK);
//		} else {
//			int statusCode = Integer.parseInt(decodedStatus);
//		
//			switch (statusCode) {
//				case 200:
//					
//					if(request.getHeader("User-Agent").contains("CloudPRNT/3.0 TSP100IV/1.1")) {
//						System.out.println("TSP100IV IS DENIED");
//					} else {
//						response = beginPostResponse(mediaTypes);
//						System.out.println("OK. Printer is online");
//					}
//
//					break;
//				case 201:
//					response = beginPostResponse(mediaTypes);
//					System.out.println("Output paper has been taken and printer is able to print.");
//					break;
//				case 211:	
//					response = beginPostResponse(mediaTypes);
//					System.out.println("Paper low.");
//					break;
//				case 410:
//					System.out.println("Out of paper");
//					break;
//				case 411:
//					System.out.println("Paper jam");
//					break;
//				case 420:
//					System.out.println("Cover open");
//					break;
//				case 510:
//					System.out.println("Incompatible media type");
//					break;
//				case 520:
//					System.out.println("Timeout");
//					break;
//				case 521:
//					System.out.println("Download failed. File is too large.");
//					break;
//				default:
//					System.out.println("Unknown Printer Status");
//			}
//					
////			** UNCOMMENT IF USING FOR HTTP CHALLENGE **
////			-- SEND HTTP CHALLENGE IF USERNAME AND PASSWORD IS SET --
////			-- Note: Must send 'Unauthorized' otherwise challenge will not be sent
////
////			if (request.getHeader("Authorization") != null) {
////				return new ResponseEntity<Model>(response, HttpStatus.OK);
////			} else {
////				responseHeaders.set("WWW-Authenticate", "Basic realm=\"Authentication Required\"");
////				return new ResponseEntity<Model>(responseHeaders, HttpStatus.UNAUTHORIZED);
////			}
//			
//			return new ResponseEntity<Model>(response, HttpStatus.OK);
	
}
